package com.thinksky.api.person.http.io;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/internal")
@ConditionalOnProperty(
        value = {"spring.cloud.kubernetes.discovery.enabled"},
        havingValue = "true",
        matchIfMissing = true
)
public class InternalController {

    private final static Logger LOGGER = LoggerFactory.getLogger(InternalController.class);

    private final WebClient webClient;
    private final DiscoveryClient discoveryClient;

    public InternalController(WebClient webClient,
                              DiscoveryClient discoveryClient) {
        this.webClient = webClient;
        this.discoveryClient = discoveryClient;
        LOGGER.info("Discover Info:" + String.join(",", discoveryClient.getServices()));
    }

    @GetMapping(path = "/get/services")
    @ResponseBody
    public List<String> getRegisteredService() {
        LOGGER.info("Call Rest Endpoint Discover Info:" + String.join(",", discoveryClient.getServices()));
        return new ArrayList<>(discoveryClient.getServices());
    }

    @GetMapping(path = "/detail/{name}")
    @ResponseBody
    public List<ServiceInstance> getServiceDeatil(@PathVariable("name") String serviceName) {
        LOGGER.info("Call Rest Endpoint Discover Info:" + String.join(",", discoveryClient.getServices()));
        return discoveryClient.getInstances(serviceName);
    }

    @GetMapping(path = "/service/person/health")
    @ResponseBody
    public Mono<String> getPersonServiceHealth() {
        return webClient
                .get()
                .uri("http://person-service-demo-service:8787/actuator/health")
                .retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> {
                    LOGGER.error("Error while calling endpoint {} with status code {}",
                            "", clientResponse.statusCode());
                    throw new RuntimeException("Error while calling  accounts endpoint");
                }).bodyToMono(String.class)
                .timeout(Duration.ofMillis(10000L));
    }

//    @GetMapping(path = "/service/person/{path}}")
//    @ResponseBody
//    public Mono<String> hitPersonServiceHealth(@PathVariable("url")String path) {
//        return webClient
//                .get()
//                .uri("http://person-service-demo-service:8787/"+path)
//                .retrieve()
//                .onStatus(HttpStatus::isError, clientResponse -> {
//                    LOGGER.error("Error while calling endpoint {} with status code {}",
//                            "", clientResponse.statusCode());
//                    throw new RuntimeException("Error while calling  accounts endpoint");
//                }).bodyToMono(String.class)
//                .timeout(Duration.ofMillis(10000L));
//    }

}
