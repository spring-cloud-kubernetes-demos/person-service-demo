package com.thinksky.api.person.http.io;


import com.thinksky.api.person.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RefreshScope
@RequestMapping("/person")
public class PersonController {

    private final static Logger LOGGER = LoggerFactory.getLogger(PersonController.class);
    private final List<Person> people;
    private final String master;

    public PersonController(@Qualifier("people") List<Person> people,
                            @Value("${person.master.name}") String master) {
        this.people = people;
        this.master = master;
    }

    @GetMapping(path = "/get")
    @ResponseBody
    public Flux<Person> getPeople() {
        LOGGER.info("Get People return it :");
        return Flux
                .fromIterable(people)
                .doOnError(throwable -> throwable.addSuppressed(PeopleNotFound()));
    }

    @GetMapping(path = "/find/{name}")
    @ResponseBody
        public Mono<Person> findPersonByName(@PathVariable("name") String name) {
        LOGGER.info("Find out the person with his/her name");
        return Flux
                .fromIterable(people)
                .filter(person -> person.getName().contains(name))
                .last();
    }

    @GetMapping(path = "/find/master")
    @ResponseBody
    public Mono<Person> findMaster() {
        LOGGER.info("Get the master of " + master);
        return Flux
                .fromIterable(people)
                .filter(person -> person.getName().contains(master))
                .last();
    }

    private Throwable PeopleNotFound() {
        return new Throwable("There is not any person in the list.");
    }

}
