package com.thinksky.api.person;

import com.thinksky.api.person.model.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableDiscoveryClient
public class PersonApplication {

    public static void main(String[] args) {

        SpringApplication.run(PersonApplication.class, args);
    }

    @Bean
    @LoadBalanced
    WebClient makeWebClient() {
        return WebClient
                .builder()
                .build();
    }

    @Bean(name = "people")
    public List<Person> makeListOfPeople() {
        List<Person> people = new ArrayList<>();
        people.add(new Person.Builder().withId(1L).withName("Person-1").withAge(30).build());
        people.add(new Person.Builder().withId(2L).withName("Person-2").withAge(45).build());
        people.add(new Person.Builder().withId(3L).withName("Person-3").withAge(68).build());
        people.add(new Person.Builder().withId(4L).withName("Person-4").withAge(34).build());
        people.add(new Person.Builder().withId(5L).withName("Person-5").withAge(87).build());

        return people;
    }

}
