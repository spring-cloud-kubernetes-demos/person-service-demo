package com.thinksky.api.person.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonAutoDetect
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class Person {

    private Long id;
    private String name;
    private int age;

    private Person(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setAge(builder.age);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    /**
     * {@code Person} builder static inner class.
     */
    public static final class Builder {
        private Long id;
        private String name;
        private int age;

        public Builder() {
        }

        public Builder(Person copy) {
            this.id = copy.getId();
            this.name = copy.getName();
            this.age = copy.getAge();
        }

        /**
         * Sets the {@code id} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code id} to set
         * @return a reference to this Builder
         */
        public Builder withId(Long val) {
            id = val;
            return this;
        }

        /**
         * Sets the {@code name} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code name} to set
         * @return a reference to this Builder
         */
        public Builder withName(String val) {
            name = val;
            return this;
        }

        /**
         * Sets the {@code age} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code age} to set
         * @return a reference to this Builder
         */
        public Builder withAge(int val) {
            age = val;
            return this;
        }

        /**
         * Returns a {@code Person} built from the parameters previously set.
         *
         * @return a {@code Person} built with parameters of this {@code Person.Builder}
         */
        public Person build() {
            return new Person(this);
        }
    }
}
